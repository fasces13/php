<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nettopalkka</title>
</head>
<body>
    <h3>Nettopalkka</h3>
    <?php
    $brutto = filter_input(INPUT_POST, "brutto", FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $pidatys = filter_input(INPUT_POST, "pidatys", FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $tyoelake = filter_input(INPUT_POST, "tyoelake", FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $vakuutus = filter_input(INPUT_POST, "vakuutus", FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);

    $pidatetty = $brutto * $pidatys / 100;
    $elaketty = $brutto * $tyoelake / 100;
    $vakuutettu = $brutto * $vakuutus / 100;
    $netto = $brutto - $pidatetty - $elaketty - $vakuutettu;
    
    printf("<p>Nettopalkka on %.2f €</p>",$netto);
    printf("<p>Ennakonpidätys on %.2f €</p>",$pidatetty);
    printf("<p>Työeläkemaksu on %.2f €</p>",$elaketty);
    printf("<p>Työttömyysvakuutusmaksu on %.2f €</p>",$vakuutettu);

    ?>
    <a href="index.html">Laske uudestaan</a>
</body>
</html>